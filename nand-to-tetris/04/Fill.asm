// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

(INFINITE)

@KBD
D=M

@CLEAR
D;JEQ

(BLACKEN)

@i
M=0

(BFILL)

@8192
D=A

@i
D=D-M

@INFINITE
D;JEQ

@i
D=M

@SCREEN
A=A+D
M=-1

@i
M=M+1

@BFILL
0;JMP

(CLEAR)

@i
M=0

(CFILL)

@8192
D=A

@i
D=D-M

@INFINITE
D;JEQ

@i
D=M

@SCREEN
A=A+D
M=0

@i
M=M+1

@CFILL
0;JMP
