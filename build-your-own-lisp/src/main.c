#include <editline/readline.h>
#include "mpc.h"
#include "lval.h"
#include "lenv.h"
#include "builtin.h"

mpc_parser_t* crisp;

int main(int argc, char **argv) {
	crisp = mpc_new("crisp");
	mpc_parser_t* integer = mpc_new("integer");
	mpc_parser_t* real    = mpc_new("real");
	mpc_parser_t* symbol  = mpc_new("symbol");
	mpc_parser_t* string  = mpc_new("string");
	mpc_parser_t* comment = mpc_new("comment");
	mpc_parser_t* sexpr   = mpc_new("sexpr");
	mpc_parser_t* qexpr   = mpc_new("qexpr");
	mpc_parser_t* expr    = mpc_new("expr");

	mpca_lang(MPCA_LANG_DEFAULT,
	"\
	integer : /-?[0-9]+/;\
	real : /-?[0-9]+[.][0-9]+/;\
	string : /\"(\\\\.|[^\"])*\"/;\
	comment: /;[^\\r\\n]*/;\
	symbol : /[a-zA-Z0-9_+\\-*\\/\\\\=<>!&]+/;\
	sexpr : '(' <expr>* ')';\
	qexpr : '{' <expr>* '}';\
	expr : <real> | <integer> | <symbol> | <string> | <comment> | <sexpr> | <qexpr>;\
	crisp : /^/ <expr>* /$/;\
	", integer, real, symbol, string, comment, sexpr, qexpr, expr, crisp);

	lenv *e = lenv_new();
	lenv_add_builtins(e);

	if (argc >= 2) {
		for (int i = 1; i < argc; i++) {
			lval *args = lval_add(lval_sexpr(), lval_str(argv[i]));
			lval *x = builtin_load(e, args);
			if (x->type == LVAL_ERR)
				lval_println(x);
			lval_delete(x);
		}
	}
	printf("crisp version NULL\nExit with Ctrl+C\n");
	while (1) {
		char *input = readline("crisp> ");
		add_history(input);

		mpc_result_t r;
		if (mpc_parse("<stdin>", input, crisp, &r)) {
			lval *read = lval_read(r.output);
			lval *x = lval_eval(e, read);
			lval_println(x);
			lval_delete(x);
			mpc_ast_delete(r.output);
		} else {
			mpc_err_print(r.error);
			mpc_err_delete(r.error);
		}

		free(input);
	}

	mpc_cleanup(9, integer, real, symbol, string, comment, sexpr, qexpr, expr, crisp);
	return 0;
}
