#include "lval.h"
#include "lenv.h"
#include "mpc.h"
#include <stdlib.h>
#include <string.h>

#define LASSERT(args, cond, fmt, ...)\
	if (!(cond)) {\
		lval *err = lval_err(fmt, ##__VA_ARGS__);\
		lval_delete(args);\
		return err;}

#define LASSERT_NUM(fun, arg, num)\
	LASSERT(arg, arg->count == num, \
			"Function '%s' passed incorrect amount of arguments" \
			"Got %i, Expected %i", fun, arg->count, num);

#define LASSERT_TYPE(fun, arg, index, exp_type) \
	LASSERT(arg, arg->cell[index]->type == exp_type, \
			"Function '%s' passed incorrect type for argument %i " \
			"Got %s, Expected %s", fun, index + 1, \
			ltype_name(a->cell[index]->type), ltype_name(exp_type)); \

extern mpc_parser_t *crisp;

lval *builtin_head(lenv *e, lval *a) {
	LASSERT_NUM ("head", a, 1);
	LASSERT_TYPE("head", a, 0, LVAL_QEXPR);
	LASSERT(a, a->cell[0]->count != 0,
			"Funtion 'head' passed {}");
	lval *v = lval_take(a, 0);
	while (v->count > 1) {
		lval_delete(lval_pop(v, 1));
	}
	return v;
}

lval *builtin_tail(lenv *e, lval *a) {
	LASSERT_NUM ("tail", a, 1);
	LASSERT_TYPE("tail", a, 0, LVAL_QEXPR);
	LASSERT(a, a->cell[0]->count != 0,
			"Function 'tail passed {}");

	lval *v = lval_take(a, 0);
	lval_delete(lval_pop(v, 0));
	return v;
}

lval *builtin_list(lenv *e, lval *a) {
	a->type = LVAL_QEXPR;
	return a;
}

lval *builtin_eval(lenv *e, lval *a) {
	LASSERT_NUM ("eval", a, 1);
	LASSERT_TYPE("eval", a, 0, LVAL_QEXPR);
	lval *x = lval_take(a, 0);
	x->type = LVAL_SEXPR;
	return lval_eval(e, x);
}

lval *builtin_join(lenv *e, lval *a) {
	for (int i = 0; i < a->count; i ++)
		LASSERT_TYPE("join", a, i, LVAL_QEXPR);

	lval *x = lval_pop(a, 0);

	while (a->count)
		x = lval_join(x, lval_pop(a, 0));

	lval_delete(a);
	return x;
}

lval *builtin_add(lenv *e, lval *a) {
	for (int i = 0; i < a->count; i ++) {
		if (a->cell[i]->type != LVAL_INT && a->cell[i]->type != LVAL_REAL) {
			lval_delete(a);
			return lval_err("Cannot operate on non-number");
		}
	}

	lval *x = lval_pop(a, 0);
	while (a->count > 0) {
		lval *y = lval_pop(a, 0);
		if (x->type == LVAL_INT && y->type == LVAL_INT)
			x->integer += y->integer;
		else if (x->type == LVAL_REAL && y->type == LVAL_INT)
			x->real += y->integer;
		else if (x->type == LVAL_INT && y->type == LVAL_REAL) {
			x->type = LVAL_REAL;
			x->real = x->integer + y->real;
		} else
			x->real += y->real;
		lval_delete(y);
	}
	lval_delete(a);
	return x;
}

lval *builtin_sub(lenv *e, lval *a) {
	for (int i = 0; i < a->count; i ++) {
		if (a->cell[i]->type != LVAL_INT && a->cell[i]->type != LVAL_REAL) {
			lval_delete(a);
			return lval_err("Cannot operate on non-number");
		}
	}

	lval *x = lval_pop(a, 0);
	if (a->count == 0) {
		if (x->type == LVAL_INT)
			x->integer = -x->integer;
		else
			x->real = -x->real;
	}

	while (a->count > 0) {
		lval *y = lval_pop(a, 0);
		if (x->type == LVAL_INT && y->type == LVAL_INT)
			x->integer -= y->integer;
		else if (x->type == LVAL_REAL && y->type == LVAL_INT)
			x->real -= y->integer;
		else if (x->type == LVAL_INT && y->type == LVAL_REAL) {
			x->type = LVAL_REAL;
			x->real = x->integer - y->real;
		} else
			x->real -= y->real;
		lval_delete(y);
	}
	lval_delete(a);
	return x;
}

lval *builtin_mul(lenv *e, lval *a) {
	for (int i = 0; i < a->count; i ++) {
		if (a->cell[i]->type != LVAL_INT && a->cell[i]->type != LVAL_REAL) {
			lval_delete(a);
			return lval_err("Cannot operate on non-number");
		}
	}

	lval *x = lval_pop(a, 0);
	while (a->count > 0) {
		lval *y = lval_pop(a, 0);
		if (x->type == LVAL_INT && y->type == LVAL_INT)
			x->integer *= y->integer;
		else if (x->type == LVAL_REAL && y->type == LVAL_INT)
			x->real *= y->integer;
		else if (x->type == LVAL_INT && y->type == LVAL_REAL) {
			x->type = LVAL_REAL;
			x->real = x->integer * y->real;
		} else
			x->real *= y->real;
		lval_delete(y);
	}
	lval_delete(a);
	return x;
}

lval *builtin_div(lenv *e, lval *a) {
	for (int i = 0; i < a->count; i ++) {
		if (a->cell[i]->type != LVAL_INT && a->cell[i]->type != LVAL_REAL) {
			lval_delete(a);
			return lval_err("Cannot operate on non-number");
		}
	}

	lval *x = lval_pop(a, 0);
	while (a->count > 0) {
		lval *y = lval_pop(a, 0);
		if ((y->type == LVAL_REAL && y->real == 0) || (y->type == LVAL_INT && y->integer == 0)) {
			lval_delete(x);
			lval_delete(y);
			x = lval_err("Division by Zero");
			break;
		}
		if (x->type == LVAL_INT && y->type == LVAL_INT)
			x->integer /= y->integer;
		else if (x->type == LVAL_REAL && y->type == LVAL_INT)
			x->real /= y->integer;
		else if (x->type == LVAL_INT && y->type == LVAL_REAL) {
			x->type = LVAL_REAL;
			x->real = x->integer / y->real;
		} else
			x->real /= y->real;
		lval_delete(y);
	}
	lval_delete(a);
	return x;
}

lval *builtin_exit(lenv *e, lval *a) {
	exit(0);
}

lval *builtin_lambda(lenv *e, lval *a) {
	LASSERT_NUM ("\\", a, 2);
	LASSERT_TYPE("\\", a, 0, LVAL_QEXPR);
	LASSERT_TYPE("\\", a, 1, LVAL_QEXPR);
	for (int i = 0; i < a->cell[0]->count; i++) {
		LASSERT(a, (a->cell[0]->cell[i]->type == LVAL_SYM),
				"Cannot define non-symbol"
				"Got %s, Expected %s", ltype_name(a->cell[0]->cell[i]->type), ltype_name(LVAL_SYM));
	}

	lval *formals = lval_pop(a, 0);
	lval *body = lval_pop(a, 0);
	lval_delete(a);

	return lval_lambda(formals, body);
}

lval *builtin_var(lenv *e, lval *a, char *func) {
	LASSERT_TYPE(func, a, 0, LVAL_QEXPR);

	lval *syms = a->cell[0];
	for (int i = 0; i < syms->count; i ++) {
		LASSERT(a, syms->cell[i]->type == LVAL_SYM,
				"Function '%s' cannot define non symbols"
				"Got %s, Expected %s", func,
				ltype_name(syms->cell[i]->type), ltype_name(LVAL_SYM));
	}

	LASSERT(a, syms->count == a->count - 1,
			"Function '%s' cannot define incorrect number of values to symbols"
			"Got %d, Expected %d",
			func, syms->count, a->count - 1);

	if (strcmp(func, "def") == 0) {
		for (int i = 0; i < syms->count; i ++) {
			lenv_def(e, syms->cell[i], a->cell[i + 1]);
		}
	} else if (strcmp(func, "=") == 0) {
		for (int i = 0; i < syms->count; i ++) {
			lenv_put(e, syms->cell[i], a->cell[i + 1]);
		}
	}

	lval_delete(a);
	return lval_sexpr();
}

lval *builtin_def(lenv *e, lval *a) {
	return builtin_var(e, a, "def");
}

lval *builtin_put(lenv *e, lval *a) {
	return builtin_var(e, a, "put");
}

enum {ORD_GT, ORD_LT, ORD_GE, ORD_LE};

static char *ord_name(int ord) {
	switch (ord) {
		case ORD_GT: return ">";
		case ORD_LT: return "<";
		case ORD_GE: return ">=";
		case ORD_LE: return "<=";
		default : return "Bad Order";
	}
}

static lval *builtin_ord(lenv *e, lval *a, int ord) {
	LASSERT_NUM(ord_name(ord), a, 2);
	LASSERT(a, a->cell[0]->type == LVAL_INT || a->cell[0]->type == LVAL_REAL,
			"Function '%s' passed incorrect type for argument 1"
			"Got %s, Expected %s or %s", ord_name(ord),
			ltype_name(a->cell[0]->type), ltype_name(LVAL_INT), ltype_name(LVAL_REAL));
	LASSERT(a, a->cell[1]->type == LVAL_INT || a->cell[1]->type == LVAL_REAL,
			"Function '%s' passed incorrect type for argument 2"
			"Got %s, Expected %s or %s", ord_name(ord),
			ltype_name(a->cell[1]->type), ltype_name(LVAL_INT), ltype_name(LVAL_REAL));

	int r;
	switch (ord) {
		case ORD_GT:
			if (a->cell[0]->type == LVAL_INT && a->cell[0]->type == LVAL_INT)
				r = a->cell[0]->integer > a->cell[1]->integer;
			else if (a->cell[0]->type == LVAL_INT && a->cell[0]->type == LVAL_REAL)
				r = a->cell[0]->integer > a->cell[1]->real;
			else if (a->cell[0]->type == LVAL_REAL && a->cell[0]->type == LVAL_INT)
				r = a->cell[0]->real > a->cell[1]->integer;
			else
				r = a->cell[0]->real > a->cell[1]->real;
			break;
		case ORD_LT:
			if (a->cell[0]->type == LVAL_INT && a->cell[0]->type == LVAL_INT)
				r = a->cell[0]->integer < a->cell[1]->integer;
			else if (a->cell[0]->type == LVAL_INT && a->cell[0]->type == LVAL_REAL)
				r = a->cell[0]->integer < a->cell[1]->real;
			else if (a->cell[0]->type == LVAL_REAL && a->cell[0]->type == LVAL_INT)
				r = a->cell[0]->real < a->cell[1]->integer;
			else
				r = a->cell[0]->real < a->cell[1]->real;
			break;
		case ORD_GE:
			if (a->cell[0]->type == LVAL_INT && a->cell[0]->type == LVAL_INT)
				r = a->cell[0]->integer >= a->cell[1]->integer;
			else if (a->cell[0]->type == LVAL_INT && a->cell[0]->type == LVAL_REAL)
				r = a->cell[0]->integer >= a->cell[1]->real;
			else if (a->cell[0]->type == LVAL_REAL && a->cell[0]->type == LVAL_INT)
				r = a->cell[0]->real >= a->cell[1]->integer;
			else
				r = a->cell[0]->real >= a->cell[1]->real;
			break;
		case ORD_LE:
			if (a->cell[0]->type == LVAL_INT && a->cell[0]->type == LVAL_INT)
				r = a->cell[0]->integer <= a->cell[1]->integer;
			else if (a->cell[0]->type == LVAL_INT && a->cell[0]->type == LVAL_REAL)
				r = a->cell[0]->integer <= a->cell[1]->real;
			else if (a->cell[0]->type == LVAL_REAL && a->cell[0]->type == LVAL_INT)
				r = a->cell[0]->real <= a->cell[1]->integer;
			else
				r = a->cell[0]->real <= a->cell[1]->real;
			break;
	}
	return lval_int(r);
}

lval *builtin_gt(lenv *e, lval *a) {return builtin_ord(e, a, ORD_GT);}
lval *builtin_lt(lenv *e, lval *a) {return builtin_ord(e, a, ORD_LT);}
lval *builtin_ge(lenv *e, lval *a) {return builtin_ord(e, a, ORD_GE);}
lval *builtin_le(lenv *e, lval *a) {return builtin_ord(e, a, ORD_LE);}

enum {EOP_EQ, EOP_NEQ};

static char * eop_name(int eop) {
	return eop == EOP_EQ ? "==" : "!=";
}

static lval *builtin_cmp(lenv *e, lval *a, int eop) {
	LASSERT_NUM(eop_name(eop), a, 2);
	int r = (eop == EOP_EQ) ?
			 lval_eq(a->cell[0], a->cell[1]):
			!lval_eq(a->cell[0], a->cell[1]);
	lval_delete(a);
	return lval_int(r);
}

lval *builtin_eq(lenv *e, lval *a) {
	return builtin_cmp(e, a, EOP_EQ);
}

lval *builtin_neq(lenv *e, lval *a) {
	return builtin_cmp(e, a, EOP_NEQ);
}

lval *builtin_if(lenv *e, lval *a) {
	LASSERT_NUM("if", a, 3);
	LASSERT_TYPE("if", a, 1, LVAL_QEXPR);
	LASSERT_TYPE("if", a, 2, LVAL_QEXPR);

	lval *x;
	a->cell[1]->type = LVAL_SEXPR;
	a->cell[2]->type = LVAL_SEXPR;

	int which;
	switch (a->cell[0]->type) {
		case LVAL_INT:
			which = a->cell[0]->integer != 0;
			break;
		case LVAL_REAL:
			which = a->cell[0]->real != 0;
			break;
		case LVAL_ERR:
			which = 0;
			break;
		case LVAL_SYM:
			which = strlen(a->cell[0]->sym) != 0;
			break;
		case LVAL_SEXPR:
		case LVAL_QEXPR:
			which = a->cell[0]->count > 0;
			break;
		case LVAL_FUN:
			which = (a->cell[0]->builtin != NULL) || (a->cell[0]->body != NULL);
			break;
		case LVAL_STR:
			which = strlen(a->cell[0]->str) > 0;
			break;
	}
	x = (which)?
		lval_eval(e, lval_pop(a, 1)):
		lval_eval(e, lval_pop(a, 2));

	lval_delete(a);
	return x;
}

lval *builtin_load(lenv *e, lval *a) {
	LASSERT_NUM("load", a, 1);
	LASSERT_TYPE("load", a, 0, LVAL_STR);

	mpc_result_t r;
	if (mpc_parse_contents(a->cell[0]->str, crisp, &r)) {
		lval *expr = lval_read(r.output);
		mpc_ast_delete(r.output);

		while (expr->count) {
			lval *x = lval_eval(e, lval_pop(expr, 0));
			if (x->type == LVAL_ERR)
				lval_println(x);
			lval_delete(x);
		}
		lval_delete(expr);
		lval_delete(a);
		return lval_sexpr();
	} else {
		char *err_msg = mpc_err_string(r.error);
		mpc_err_delete(r.error);

		lval *err = lval_err("Could not load Library %s", err_msg);
		free(err_msg);
		lval_delete(a);

		return err;
	}
}

lval *builtin_print(lenv *e, lval *a) {
	for (int i = 0; i < a->count; i ++) {
		lval_print(a->cell[i]);
		putchar(' ');
	}
	putchar('\n');
	lval_delete(a);
	return lval_sexpr();
}

lval *builtin_error(lenv *e, lval *a) {
	LASSERT_NUM ("error", a, 1);
	LASSERT_TYPE("error", a, 0, LVAL_STR);

	lval *err = lval_err(a->cell[0]->str);

	lval_delete(a);
	return err;
}
