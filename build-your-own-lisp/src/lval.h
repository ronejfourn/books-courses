enum {LVAL_INT, LVAL_REAL, LVAL_ERR, LVAL_SYM, LVAL_SEXPR, LVAL_QEXPR, LVAL_FUN, LVAL_STR};

typedef struct lval lval;
typedef struct lenv lenv;
typedef struct mpc_ast_t mpc_ast_t;
typedef lval*(*lbuiltin)(lenv*, lval*);

lval *lval_int (long x);
lval *lval_err (char *fmt, ...);
lval *lval_lambda(lval *formals, lval *body);
lval *lval_str(char *s);
lval *lval_sym (char *s);
lval *lval_fun(lbuiltin func);
lval *lval_add(lval *v, lval *x);
lval *lval_sexpr(void);
void lval_delete(lval *v);
lval *lval_copy(lval *v);
lval *lval_pop(lval *v, int i);
lval *lval_take(lval *v, int i);
lval *lval_join(lval *x, lval *y);
int lval_eq(lval *x, lval *y);
lval *lval_read(mpc_ast_t *t);
lval *lval_eval(lenv *e, lval *v);
char *ltype_name(int t);
void lval_print(lval* v);
void lval_println(lval *v);

struct lval{
	int type;
	union{
		long integer;
		double real;
		char *err;
		char *sym;
		char *str;
		struct {
			int count;
			struct lval **cell;
		};
		struct {
			lbuiltin builtin;
			lenv *env;
			lval *formals;
			lval *body;
		};
	};
};
