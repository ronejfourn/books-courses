typedef struct lval lval;
typedef struct lenv lenv;
typedef lval*(*lbuiltin)(lenv*, lval*);

lenv *lenv_new(void);
void lenv_delete(lenv *e);
lval *lenv_get(lenv *e, lval*k);
void lenv_put(lenv *e, lval *k, lval *v);
void lenv_def(lenv *e, lval *k, lval *v);
lenv *lenv_copy(lenv *e);
void lenv_add_builtins(lenv *e);

struct lenv {
	lenv *par;
	int count;
	char **syms;
	lval **vals;
};

