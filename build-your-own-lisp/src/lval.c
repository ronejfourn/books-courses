#include "lval.h"
#include "lenv.h"
#include "mpc.h"
#include "builtin.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

//
// Constructors
//

lval *lval_int (long x) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_INT;
	v->integer = x;
	return v;
}

static lval *lval_real(double x) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_REAL;
	v->real = x;
	return v;
}

lval *lval_err (char *fmt, ...) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_ERR;

	va_list va, vc;
	va_start(va, fmt);
	va_copy(vc, va);
	int len = vsnprintf(NULL, 0, fmt, va);
	v->err = malloc(len + 1);
	vsnprintf(v->err, 511, fmt, vc);
	v->err[len] = 0;
	va_end(va);
	return v;
}

lval *lval_sym (char *s) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_SYM;
	v->err = malloc(strlen(s) + 1);
	strcpy(v->err, s);
	return v;
}

lval *lval_sexpr(void) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_SEXPR;
	v->count = 0;
	v->cell = NULL;
	return v;
}

static lval *lval_qexpr(void) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_QEXPR;
	v->count = 0;
	v->cell = NULL;
	return v;
}

lval *lval_fun(lbuiltin func) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_FUN;
	v->builtin = func;
	return v;
}

lval *lval_lambda(lval *formals, lval *body) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_FUN;

	v->builtin = NULL;
	v->env = lenv_new();

	v->formals = formals;
	v->body    = body;
	return v;
}

lval *lval_str(char *s) {
	lval *v = malloc(sizeof(lval));
	v->type = LVAL_STR;
	v->str = malloc(strlen(s) + 1);
	strcpy(v->str, s);
	return v;
}

//
// Utilities
//

void lval_delete(lval *v) {
	switch (v->type) {
		case LVAL_REAL: break;
		case LVAL_INT : break;
		case LVAL_ERR : free(v->err); break;
		case LVAL_SYM : free(v->sym); break;
		case LVAL_QEXPR:
		case LVAL_SEXPR:
			for (int i = 0; i < v->count; i ++) {
				lval_delete(v->cell[i]);
			}
			free(v->cell);
			break;
		case LVAL_FUN:
			if (!v->builtin) {
				lenv_delete(v->env);
				lval_delete(v->formals);
				lval_delete(v->body);
			}break;
		case LVAL_STR:
			free(v->str);
			break;
	}
	free(v);
}

lval *lval_copy(lval *v) {
	lval *x = malloc(sizeof(lval));
	x->type = v->type;

	switch (v->type) {
		case LVAL_FUN:
			if (v->builtin) {
				x->builtin = v->builtin;
			} else {
				x->builtin = NULL;
				x->env = lenv_copy(v->env);
				x->formals = lval_copy(v->formals);
				x->body = lval_copy(v->body);
			}break;
		case LVAL_INT: x->integer = v->integer; break;
		case LVAL_REAL: x->real = v->real; break;
		case LVAL_ERR:
			x->err = malloc(strlen(v->err) + 1);
			strcpy(x->err, v->err); break;
		case LVAL_SYM:
			x->sym = malloc(strlen(v->sym) + 1);
			strcpy(x->sym, v->sym); break;
		case LVAL_SEXPR:
		case LVAL_QEXPR:
			x->count = v->count;
			x->cell = malloc(sizeof(lval *) * x->count);
			for (int i = 0; i < x->count; i++) {
				x->cell[i] = lval_copy(v->cell[i]);
			} break;
		case LVAL_STR:
			x->str = malloc(strlen(v->str) + 1);
			strcpy(x->str, v->str);
			break;
	}
	return x;
}

lval *lval_add(lval *v, lval *x) {
	v->count ++;
	v->cell = realloc(v->cell, sizeof(lval *) * v->count);
	v->cell[v->count - 1] = x;
	return v;
}

lval *lval_pop(lval *v, int i) {
	lval *x = v->cell[i];

	memmove(&v->cell[i], &v->cell[i + 1],
		sizeof(lval *) * (v->count - i - 1));

	v->count --;

	v->cell = realloc(v->cell, sizeof(lval *) * v->count);
	return x;
}

lval *lval_take(lval *v, int i) {
	lval *x = lval_pop(v, i);
	lval_delete(v);
	return x;
}

lval *lval_join(lval *x, lval *y) {
	while (y->count) {
		x = lval_add(x, lval_pop(y, 0));
	}
	lval_delete(y);
	return x;
}

int lval_eq(lval *x, lval *y) {
	if (x->type == LVAL_INT && y->type == LVAL_REAL)
		return x->integer == y->real;
	if (y->type == LVAL_INT && x->type == LVAL_REAL)
		return y->integer == x->real;

	if (x->type != y->type)
		return 0;

	switch (x->type) {
		case LVAL_INT : return x->integer == y->integer;
		case LVAL_REAL: return x->real == y->real;
		case LVAL_ERR : return strcmp(x->err, y->err) == 0;
		case LVAL_SYM : return strcmp(x->sym, y->sym) == 0;
		case LVAL_FUN :
			if (x->builtin || y->builtin) {
				return x->builtin == y->builtin;
			} else {
				return lval_eq(x->formals, y->formals) &&
					lval_eq(x->body, y->body);
			}
		case LVAL_QEXPR:
		case LVAL_SEXPR:
			if (x->count != y->count)
				return 0;
			for (int i = 0; i < x->count; i++)
				if (!lval_eq(x->cell[i], y->cell[i]))
					return 0;
			return 1;
		case LVAL_STR:
			return (strcmp(x->str, y->str) == 0);
	}

	return 0;
}

//
// Reading
//

static lval *lval_read_int(mpc_ast_t* t) {
	errno = 0;
	long ret = strtol(t->contents, NULL, 10);
	return errno ? lval_err("Invalid Number") : lval_int(ret);
}

static lval *lval_read_real(mpc_ast_t* t) {
	errno = 0;
	double ret = strtod(t->contents, NULL);
	return errno ? lval_err("Invalid Number") : lval_real(ret);
}

static lval *lval_read_str(mpc_ast_t *t) {
	t->contents[strlen(t->contents) - 1] = 0;
	char *unescaped = malloc(strlen(t->contents + 1) + 1);
	strcpy(unescaped, t->contents + 1);
	unescaped = mpcf_unescape(unescaped);
	lval *str = lval_str(unescaped);
	free(unescaped);
	return str;
}

lval *lval_read(mpc_ast_t *t) {
	if (strstr(t->tag, "integer")) return lval_read_int(t);
	if (strstr(t->tag, "real"))    return lval_read_real(t);
	if (strstr(t->tag, "symbol"))  return lval_sym(t->contents);
	if (strstr(t->tag, "string"))  return lval_read_str(t);

	lval *x = NULL;
	if (strcmp(t->tag, ">") == 0 || strstr(t->tag, "sexpr"))
		x = lval_sexpr();
	if (strstr(t->tag, "qexpr"))
		x = lval_qexpr();

	for (int i = 0; i < t->children_num; i ++) {
		if (strcmp(t->children[i]->contents, "(" ) == 0) continue;
		if (strcmp(t->children[i]->contents, ")" ) == 0) continue;
		if (strcmp(t->children[i]->contents, "{" ) == 0) continue;
		if (strcmp(t->children[i]->contents, "}" ) == 0) continue;
		if (strcmp(t->children[i]->tag, "regex"  ) == 0) continue;
		if (strstr(t->children[i]->tag, "comment")) continue;
		x = lval_add(x, lval_read(t->children[i]));
	}
	return x;
}

//
// Evaluation
//

static lval *lval_call(lenv *e, lval *f, lval *a) {
	if (f->builtin)
		return f->builtin(e, a);

	int given = a->count;
	int total = f->formals->count;

	while (a->count) {
		if (f->formals->count == 0) {
			lval_delete(a);
			lval_err(
					"Function passed too many arguments."
					"Got %i, Expected %i", given, total);
		}
		lval *sym = lval_pop(f->formals, 0);
		lval *val = lval_pop(a, 0);

		if (strcmp(sym->sym, "&") == 0) {
			if (f->formals->count != 1) {
				lval_delete(a);
				return lval_err("Function format invalid."
						"Symbol '&' not followed by single symbol");
			}
			lval *nsym = lval_pop(f->formals, 0);
			lenv_put(f->env, nsym, builtin_list(e, a));
			lval_delete(sym);
			lval_delete(nsym);
			break;
		}

		lenv_put(f->env, sym, val);
		lval_delete(sym);
		lval_delete(val);
	}
	lval_delete(a);
	if (f->formals->count > 0 && strcmp(f->formals->cell[0]->sym, "&") == 0) {
		if (f->formals->count != 2) {
			return lval_err("Function format invalid."
					"Symbol '&' not followed by single symbol");
		}

		lval_delete(lval_pop(f->formals, 0));
		lval *sym = lval_pop(f->formals, 0);
		lval *val = lval_qexpr();

		lenv_put(f->env, sym, val);
		lval_delete(sym);
		lval_delete(val);
	}
	if (f->formals->count == 0) {
		f->env->par = e;
		return builtin_eval(f->env, lval_add(lval_sexpr(), lval_copy(f->body)));
	} else {
		return lval_copy(f);
	}
}

static lval *lval_eval_sexpr(lenv *e, lval *v) {
	for (int i = 0; i < v->count; i ++)
		v->cell[i] = lval_eval(e, v->cell[i]);

	for(int i = 0; i < v->count; i ++)
		if (v->cell[i]->type == LVAL_ERR)
			return lval_take(v, i);

	if (v->count == 0)
		return v;

	if (v->count == 1)
		return lval_take(v, 0);

	lval *f = lval_pop(v, 0);
	if (f->type != LVAL_FUN) {
		lval_delete(f);
		lval_delete(v);
		return lval_err("First element is not a function");
	}

	lval *result = lval_call(e, f,v);
	lval_delete(f);
	return result;
}

lval *lval_eval(lenv *e, lval *v) {
	if (v->type == LVAL_SYM) {
		lval *x = lenv_get(e, v);
		lval_delete(v);
		return x;
	}
	if (v->type == LVAL_SEXPR) {
		return lval_eval_sexpr(e, v);
	}
	return v;
}

//
// Printing
//

char *ltype_name(int t) {
	switch(t) {
		case LVAL_FUN  : return "function";
		case LVAL_REAL : return "real";
		case LVAL_INT  : return "integer";
		case LVAL_ERR  : return "error";
		case LVAL_SYM  : return "symbol";
		case LVAL_SEXPR: return "S-Expression";
		case LVAL_QEXPR: return "Q-Expression";
		case LVAL_STR  : return "string";
		default: return "unknown";
	}
}

static void lval_expr_print(lval *v, char open, char close) {
	putchar(open);
	for (int i = 0; i < v->count; i ++) {
		lval_print(v->cell[i]);
		putchar(' ');
	}
	if (v->count > 0)
		putchar('\b');
	putchar(close);
}

static void lval_print_str(lval *v) {
	char *escaped = malloc(strlen(v->str) + 1);
	strcpy(escaped, v->str);
	escaped = mpcf_escape(escaped);
	printf("\"%s\"", escaped);
	free(escaped);
}

void lval_print(lval *v) {
	switch(v->type) {
		case LVAL_INT : printf("%ld", v->integer); break;
		case LVAL_REAL: printf("%f", v->real); break;
		case LVAL_ERR : printf("ERROR: %s", v->err); break;
		case LVAL_SYM : printf("%s", v->sym); break;
		case LVAL_FUN :
			if (v->builtin) {
				printf("<builtin>");
			} else {
				printf("(\\ ");
				lval_print(v->formals);
				putchar(' ');
				lval_print(v->body);
				putchar(')');
			} break;
		case LVAL_SEXPR: lval_expr_print(v, '(', ')'); break;
		case LVAL_QEXPR: lval_expr_print(v, '{', '}'); break;
		case LVAL_STR  : lval_print_str(v); break;
	}
}

void lval_println(lval *v) { lval_print(v); putchar('\n'); }
