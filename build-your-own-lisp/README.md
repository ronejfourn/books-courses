# Crisp

Simple interpreter for a lispy language,
built following the [Build your own Lisp](http://buildyourownlisp.com/) book.

### Differences from the book
- support for floating point numbers
- (), {}, "", 0, 0.0 all count as false in `if` function

### Compiling
Run `make`
